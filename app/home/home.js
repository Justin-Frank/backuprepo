'use strict';
 
angular.module('myApp.home', ['ngRoute'])
 
// Declared route 
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'home/home.html',
        controller: 'HomeCtrl'
    });
}])
 
// Primary controller
.controller('HomeCtrl', ['$scope', function($scope) {

	$scope.sampleText = 'Enter some sample text';
	$scope.reset = '';

	function wordFreq(string) {
	    var words = string.replace(/[.]/g, '').split(/\s/);
	    var freqMap = {};
	    words.forEach(function(word) {
	        if (!freqMap[word]) {
	            freqMap[word] = 0;
	        }
	        freqMap[word] += 1;
	    });
	    
	    return freqMap;
	}

	$scope.totalFrequency = wordFreq($scope.sampleText);

	$scope.textSummary = Object.keys($scope.totalFrequency).sort().forEach(function(word) {
	    console.log(word + ' appears ' + $scope.totalFrequency[word] + ' time ');
	});
}]);